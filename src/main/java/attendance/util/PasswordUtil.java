package attendance.util;

import java.security.SecureRandom;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.util.Base64Utils;

public class PasswordUtil {

	private static final int saltLen = 32;
	private static final int iterations = 1;
	private static final int desiredKeyLen = 256;

	public static String getSaltedHash(String password) throws Exception{
		byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen);
		return Base64Utils.encodeToString(salt)+"$"+hash(password,salt);
	}

	private static String hash(String password, byte[] salt) throws Exception{

		if(password == null || password.length() == 0)
			throw new IllegalArgumentException("Empty passwords are not supported");

		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey key = f.generateSecret(new PBEKeySpec(password.toCharArray(),salt,iterations,desiredKeyLen));
		return Base64Utils.encodeToString(key.getEncoded()); 
	}

	public static boolean check(String password, String existingPasswordinDB) throws Exception{
		String[] saltAndPass = existingPasswordinDB.split("\\$");
		if(saltAndPass.length !=2) {
			throw new IllegalArgumentException("The Stored password should have the form 'salt$hash'");
		}

		String hashOfInput = hash(password, Base64Utils.decodeFromString(saltAndPass[0]));
		return hashOfInput.equals(saltAndPass[1]);
	}

}
