package attendance.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import attendance.dao.GradeDao;
import attendance.dao.ParentDao;
import attendance.model.Parent;
import attendance.model.Student;

@Component
public class FileReaderUtil {

	@Autowired
	private GradeDao gradeDao;

	@Autowired
	private ParentDao parentDao;

	public List<Student> saveStudentData(String filePath) {
		try {

			List<Student> students = new ArrayList<>();
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);

			XSSFWorkbook xwb = new XSSFWorkbook(fis);

			XSSFSheet studentSheet = xwb.getSheetAt(0);

			Iterator<Row> rowIterator = studentSheet.rowIterator();

			while (rowIterator.hasNext()) {

				Row row = rowIterator.next();

				Student s = new Student();				
				s.setCreatedTimeStamp(new Date().getTime());
				s.setDateOfBirth(new Date());
				s.setEffectiveStatus("ACTIVE");
				s.setFirstName(row.getCell(0).getStringCellValue());
				s.setLastName(row.getCell(1).getStringCellValue());

				s.setGrade(gradeDao.findByclassName(row.getCell(2).getStringCellValue()));

				if(row.getCell(5) != null) {
					Parent parentByPhoneNumber = parentDao.findByphoneNumber(String.valueOf(row.getCell(5).getNumericCellValue()));
					Parent parentByFirstNameAndLastName = parentDao.findByFirstNameAndLastName(row.getCell(3).getStringCellValue(), row.getCell(4).getStringCellValue());
					if(parentByPhoneNumber != null) {
						s.setParent(parentByPhoneNumber);
					}else if(parentByFirstNameAndLastName != null && parentByPhoneNumber == null){
						s.setParent(parentByFirstNameAndLastName);
					}else {
						Parent parent = new Parent();
						parent.setParentId(getUniqueId());
						parent.setFirstName(row.getCell(3).getStringCellValue());
						parent.setLastName(row.getCell(4).getStringCellValue());
						if(row.getPhysicalNumberOfCells() > 6)
							parent.setAddress(row.getCell(6).getStringCellValue());
						parentDao.save(parent);
						s.setParent(parent);
					}
				}
				s.setStudentId(getUniqueId());
				students.add(s);
			}

			xwb.close();
			return students;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String getUniqueId() {
		String uuid = UUID.randomUUID().toString();
		return uuid+new Date().getTime();
	}

}
