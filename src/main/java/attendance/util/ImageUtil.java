package attendance.util;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import attendance.model.Student;
import attendance.service.AttendanceService;

@Component
public class ImageUtil {
	
	private Logger logger = LogManager.getLogger(ImageUtil.class.getName());
	
	@Value("${IMAGES_PATH}")
	private String imageBasePath;
	
	@Value("${IMAGE_IP}")
	private String imageIp;
	
	@Value("${IMAGE_PORT}")
	private String imagePort;
	
	@Value("${IMAGE_CONTEXT}")
	private String imageContext;
	
	@Autowired
	private AttendanceService attendanceService;
	
	public String saveImage(MultipartFile file, Student folderBasedonUserObject){
		String imagePath = null;
		if(folderBasedonUserObject != null && folderBasedonUserObject.getStudentId() != null && !folderBasedonUserObject.getStudentId().trim().isEmpty()){
			imagePath = imageBasePath+File.separator+folderBasedonUserObject.getStudentId();
		}else{
			imagePath = imageBasePath+File.separator;
		}
		
		if(!new File(imagePath).exists())
			new File(imagePath).mkdirs();
		
		try {
			File imageFile = new File(imagePath+File.separator+file.getOriginalFilename());
			file.transferTo(imageFile);
			
			String imageUrl = buildUrl(file.getOriginalFilename(), folderBasedonUserObject.getStudentId());
			
			boolean isUpdated = attendanceService.updateStudent(folderBasedonUserObject, imageUrl);
			if(isUpdated)
				return imageUrl;

		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	private String buildUrl(String fileName, String userId){
		String url = imageIp+":"+imagePort+"/"+userId+"/"+fileName;
		logger.info(url);
		return url;
	}

}
