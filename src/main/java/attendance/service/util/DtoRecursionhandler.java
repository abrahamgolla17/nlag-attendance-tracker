package attendance.service.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import attendance.model.Campus;
import attendance.model.Church;
import attendance.model.Grade;
import attendance.model.Parent;
import attendance.model.Student;
import attendance.model.Teacher;

@Component
public class DtoRecursionhandler {

	public Teacher getTeacher(Teacher teacher) {
		if(teacher != null) {
			Teacher teacherObject = new Teacher();
			teacherObject.setFirstName(teacher.getFirstName());
			teacherObject.setImageUrl(teacher.getImageUrl());
			teacherObject.setLastName(teacher.getLastName());
			teacherObject.setPhoneNumber(teacher.getPhoneNumber());
			teacherObject.setTeacherId(teacher.getTeacherId());
			
			Set<Grade> classes = new HashSet<>();
			
			teacher.getClasses().forEach(grade -> {
				classes.add(getGrade(grade));
			});
			
			teacherObject.setClasses(classes);
			
			return teacherObject;
		}
		return null;
	}

	public Grade getGrade(Grade grade) {
		if(grade != null) {
			Grade gradeObj = new Grade();
			gradeObj.setCampus(null);
			gradeObj.setClassId(grade.getClassId());
			gradeObj.setClassName(grade.getClassName());
			gradeObj.setTeachers(null);
			return gradeObj;	
		}return null;
	}

	public Church getChurch(Church c) {
		if( c != null) {
			Church church = new Church();
			church.setAddress(c.getAddress());
			church.setCampuses(null);
			church.setChurchGeoCoordinates(c.getChurchGeoCoordinates());
			church.setChurchId(c.getChurchId());
			church.setName(c.getName());
			List<Campus> campuses = new ArrayList<>();
			c.getCampuses().forEach( campus -> {
				campuses.add(getCampus(campus));
			});
			church.setCampuses(campuses);
			return church;
		}
		return null;
	}

	public Campus getCampus(Campus campus) {

		if(campus != null) {
			Campus c = new Campus();
			c.setCampusAddress(campus.getCampusAddress());
			c.setCampusGeoCoordinates(campus.getCampusGeoCoordinates());
			c.setCampusId(campus.getCampusId());
			c.setCampusName(campus.getCampusName());
			c.setChurch(null);
			List<Grade> grades = new ArrayList<>();
			campus.getGrades().forEach(grade -> {
				grades.add(getGrade(grade));
			});
			c.setGrades(grades);
			return c;
		}
		return null;
	}

	public Student getStudent(Student student) {
		if(student != null) {
			Student s = new Student();
			s.setDateOfBirth(student.getDateOfBirth());
			s.setFirstName(student.getFirstName());
			s.setImageUrl(student.getImageUrl());
			s.setLastName(student.getLastName());
			s.setParent(getParent(student.getParent()));
			s.setStudentId(student.getStudentId());
			return s;
		}
		return null;
	}

	private Parent getParent(Parent parent) {
		if(parent != null) {
			Parent p = new Parent();
			p.setAddress(parent.getAddress());
			p.setAltPhoneNumber(parent.getAltPhoneNumber());
			p.setFirstName(parent.getFirstName());
			p.setLastName(parent.getLastName());
			p.setParentId(parent.getParentId());
			p.setParentType(parent.getParentType());
			p.setPhoneNumber(parent.getPhoneNumber());
			return p;
		}
		return null;
	}

}
