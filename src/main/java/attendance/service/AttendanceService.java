package attendance.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import attendance.model.Grade;
import attendance.model.Student;
import attendance.model.Teacher;

public interface AttendanceService {

	public Map<String, Object> validateUserAndGetData(String userDetails);
	public Teacher validateTeacher(String teacherId);
	public Grade validateClass(String gradeId);
	public List<Student> getStudentsByClassId(String classId);
	public boolean recordAttendance(JSONObject object,Teacher teacher, Grade grade);
	public boolean isValidUser(String userDetails);
	public Student validateStudent(String studentId);
	public boolean updateStudent(Student folderBasedonUserObject, String imageUrl);
	public boolean addStudentData(String filePath);
}
