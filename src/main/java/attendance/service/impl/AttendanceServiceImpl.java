package attendance.service.impl;

import java.net.URI;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import attendance.dao.ChurchDao;
import attendance.dao.ClassDoa;
import attendance.dao.StudentAttendanceDao;
import attendance.dao.StudentDao;
import attendance.dao.TeacherDao;
import attendance.dao.UserDao;
import attendance.model.Church;
import attendance.model.Grade;
import attendance.model.Student;
import attendance.model.StudentAttendance;
import attendance.model.Teacher;
import attendance.model.UserAuth;
import attendance.service.AttendanceService;
import attendance.service.util.DtoRecursionhandler;
import attendance.util.FileReaderUtil;
import attendance.util.PasswordUtil;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService{

	@Autowired
	private UserDao userDao;

	@Autowired
	private ChurchDao churchDao;

	@Autowired
	private TeacherDao teacherDao;

	@Autowired
	private ClassDoa classDao;

	@Autowired
	private StudentDao studentDao;

	@Autowired
	private DtoRecursionhandler recursionHandler;

	@Autowired
	private StudentAttendanceDao studentAttendanceDao;

	@Autowired
	private FileReaderUtil fileReaderUtil;

	private Logger logger = LogManager.getLogger(AttendanceServiceImpl.class);

	@Override
	public Map<String, Object> validateUserAndGetData(String userDetails) {


		Map<String, Object> values = new HashMap<>();
		try {
			if(isValidUser(userDetails)) {
				String[] userInfo = userDetails.split(":");
				String mobileNumber = userInfo[0];
				Teacher teacher = teacherDao.findByphoneNumber(mobileNumber);
				values.put("teacher", recursionHandler.getTeacher(teacher));
				Iterable<Church> churches = churchDao.findAll();
				List<Church> churchesList = new ArrayList<>();
				churches.forEach(c -> {
					churchesList.add(recursionHandler.getChurch(c));
				});
				values.put("church", churchesList);
				return values;
			}
		}catch (Exception e) {
			logger.error(e.getLocalizedMessage());
			return null;
		}
		return values;
	}

	@Override
	public boolean isValidUser(String userDetails) {
		logger.info("Validating user ");

		String[] userInfo = userDetails.split(":");

		if(userInfo.length > 1) {

			String mobileNumber = userInfo[0];
			String password = userInfo[1];

			if(mobileNumber.length() < 10) 
				throw new IllegalArgumentException("Please enter valid mobile number");

			UserAuth user = userDao.findBymobileNumber(mobileNumber);

			if(user == null)
				throw new IllegalArgumentException("Please enter valid mobile number");


			try {
				boolean valid = PasswordUtil.check(password, user.getPassword());
				return valid;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

		}
		return false;
	}

	@Override
	public Teacher validateTeacher(String teacherId) {
		logger.info("Check if teacher exits with id "+ teacherId);
		Teacher teacher = teacherDao.findByTeacherId(teacherId);
		if(teacher == null) {
			logger.error("Teacher with id "+teacherId+" doesnt exist");
			throw new RuntimeException("Invalid Teacher details");
		}
		return teacher;
	}

	@Override
	public Grade validateClass(String gradeId) {
		logger.info("Check if class exits with id "+ gradeId);
		Grade grade = classDao.findByclassId(gradeId);
		if(grade == null) {
			logger.error("Class with id "+gradeId+" doesnt exist");
			throw new RuntimeException("Invalid Class details");
		}
		return grade;
	}

	@Override
	public List<Student> getStudentsByClassId(String classId) {
		logger.info("Get Students by "+ classId);
		List<Student> students = studentDao.findByGrade_classId(classId);

		if(students != null) {
			List<Student> studentsList = new ArrayList<>();
			students.forEach(student -> {
				studentsList.add(recursionHandler.getStudent(student));
			});
			return studentsList;
		}
		return null;
	}

	@Override
	public Student validateStudent(String studentId) {
		Student student = studentDao.findBystudentId(studentId);
		if(student != null)
			return student;
		return null;
	}

	@Override
	public boolean recordAttendance(JSONObject studentObject, Teacher teacher, Grade grade) {

		logger.info("Recording Attendance "+ studentObject);


		JSONArray studentArray = (JSONArray) studentObject.get("students");

		long attendanceTimeStamp = studentObject.getLong("date");
		int week = getWeeknumber(attendanceTimeStamp);
		String month = getMonth(attendanceTimeStamp);
		Date date = new Date(attendanceTimeStamp);
		boolean updated = false;

		for(int i = 0; i < studentArray.length(); i++) {
			String studentId =studentArray.getString(i);
			Student s = studentDao.findBystudentId(studentId);
			StudentAttendance existingStudentAttendance =  studentAttendanceDao.findByMonthAndGrade_ClassIdAndStudent_StudentId(month, grade.getClassId(), s.getStudentId());
			if(existingStudentAttendance != null) {

				switch (week) {
				case 1:
					existingStudentAttendance.setWeek1("PRESENT");
					break;
				case 2:
					existingStudentAttendance.setWeek2("PRESENT");
					break;
				case 3: 
					existingStudentAttendance.setWeek3("PRESENT");
					break;
				case 4: 
					existingStudentAttendance.setWeek4("PRESENT");
					break;
				case 5:
					existingStudentAttendance.setWeek5("PRESENT");
					break;
				default:
					break;
				}
				try {
					existingStudentAttendance.setDate(date);
					studentAttendanceDao.save(existingStudentAttendance);
					updated = true;
				}catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("Failed to save attendance information "+e.getLocalizedMessage());
				}

			}else {
				StudentAttendance studentAttendance = new StudentAttendance();
				studentAttendance.setStudentAttendanceId(getUniqueId());
				studentAttendance.setYear(Year.now().getValue());
				studentAttendance.setDate(date);
				studentAttendance.setGrade(grade);
				studentAttendance.setMonth(month);
				switch (week) {
				case 1:
					studentAttendance.setWeek1("PRESENT");
					break;
				case 2:
					studentAttendance.setWeek2("PRESENT");
					break;
				case 3: 
					studentAttendance.setWeek3("PRESENT");
					break;
				case 4: 
					studentAttendance.setWeek4("PRESENT");
					break;
				case 5:
					studentAttendance.setWeek5("PRESENT");
					break;
				default:
					break;
				}
				studentAttendance.setStudent(s);
				try {
					studentAttendanceDao.save(studentAttendance);
					updated = true;
				}catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException("Failed to save attendance information "+e.getLocalizedMessage());
				}
			}
		}

		return updated;
	}

	@Override
	public boolean updateStudent(Student student, String imageUrl) {
		try {
			URI uri = new URI(imageUrl);
			student.setImageUrl(uri.getPath());
			student.setUpdatedDateStamp(new Date().getTime());
			studentDao.save(student);
			return true;
		}catch (Exception e) {
			logger.error("caught exception ",e);
			return false;
		}
	}

	@Override
	public boolean addStudentData(String filePath) {
		logger.info("Adding student data from "+filePath);
		try {
			List<Student> students = fileReaderUtil.saveStudentData(filePath);
			if(students != null) {
				Iterable<Student> updatedStudents = studentDao.saveAll(students);
				if(updatedStudents != null)
					return true;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return false;
	}

	private int getWeeknumber(long timeStamp) {
		Date date = new Date(timeStamp);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.WEEK_OF_MONTH);

		return week;
	}

	private String getMonth(long timeStamp) {
		Date date = new Date(timeStamp);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
	}

	private String getUniqueId() {
		String uuid = UUID.randomUUID().toString();
		return uuid+new Date().getTime();
	}

}
