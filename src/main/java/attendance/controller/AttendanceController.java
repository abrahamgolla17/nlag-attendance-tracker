package attendance.controller;

import org.springframework.http.ResponseEntity;

import attendance.util.Response;

public interface AttendanceController {


	public ResponseEntity<Response> login(String authorization, String appId);
	public ResponseEntity<Response> getStudentData(String teacherId, String classId);
	public ResponseEntity<Response> recordAttendance(String teacherId,String classId, String studentData, boolean bulk);
	
	
	public ResponseEntity<String> addUserData(String userId, String userData, boolean teacher, boolean bulk);
	public ResponseEntity<String> getStatistics(String userId, String year,String month,String studentId, String teacherId);

}
