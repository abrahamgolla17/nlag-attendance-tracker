package attendance.controller.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import attendance.controller.CommonController;
import attendance.model.Student;
import attendance.service.AttendanceService;
import attendance.util.ImageUtil;
import attendance.util.Response;

@RestController
public class CommonControllerImpl implements CommonController{

	private Logger logger = LogManager.getLogger(CommonControllerImpl.class);

	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private ImageUtil imageUtil;

	@RequestMapping(path="/profile/{studentid}", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> imageUpload(
			@RequestHeader String authorization,
			@PathVariable(name = "studentid")String studentId,
			MultipartFile files,
			HttpServletRequest request,
			@RequestParam(required = false) boolean student) {

		logger.info("/upload image for "+studentId);
		Response response = new Response();

		if(authorization == null || authorization.trim().isEmpty()){
			response.setStatus("FAILURE");
			response.setData("Unauthorized");
			response.setStatusCode(401);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if(authorization != null && authorization.contains("Basic")) {
			byte[] decodedBytes = Base64.getDecoder().decode(authorization.substring("Basic".length()).trim());
			String decodedValue = new String(decodedBytes);

			try {
				boolean isValid = attendanceService.isValidUser(decodedValue);
				if(isValid) {
					Student studentObject = attendanceService.validateStudent(studentId);
					if(student) {
						String imageUrl = imageUtil.saveImage(files, studentObject);
						response.setData(imageUrl);
						response.setStatus("SUCCESS");
						response.setStatusCode(200);
						return ResponseEntity.ok().body(response);
					}
				}else {
					response.setData("Please enter valid credentials");
					response.setStatus("FAILURE");
					response.setStatusCode(401);
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
				}
			}catch (Exception e) {
				logger.error(e.getLocalizedMessage());
				response.setData(e.getLocalizedMessage());
				response.setStatus("FAILURE");
				response.setStatusCode(503);
				return ResponseEntity.badRequest().body(response);
			}
		}

		return null;
	}

	@RequestMapping(path="/updatedata", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> uploadData(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="type") String type,
			MultipartFile file,
			HttpServletRequest request, 
			@RequestParam(name="console", required=true, defaultValue="false")  boolean console) {

		Response response = new Response();
		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setStatus("FAILURE");
			response.setData("Unauthorized");
			response.setStatusCode(401);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if(file == null){
			response.setData("please upload valid files");
			response.setStatus("FAILURE");
			response.setStatusCode(400);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		if (!file.isEmpty()) {
			try {

				byte[] bytes = file.getBytes();
				String filePath = System.getProperty("java.io.tmpdir")+File.separator+file.getOriginalFilename();
				Path path = Paths.get(filePath);
				Files.write(path, bytes);

				if(type.equalsIgnoreCase("student")) {
					try {
						boolean isAdded = attendanceService.addStudentData(filePath);
						if(isAdded) {
							response.setData("Data added successfully");
							response.setStatus("SUCCESS");
							response.setStatusCode(200);
							return ResponseEntity.ok().body(response);
						}else {
							response.setData("Failed to add data, please contact adminstrator");
							response.setStatus("FAILURE");
							response.setStatusCode(400);
							return ResponseEntity.badRequest().body(response);
						}
					}catch (Exception e) {
						logger.error("Caught ",e);
						response.setData(e.getLocalizedMessage());
						response.setStatus("FAILURE");
						response.setStatusCode(503);
						return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);


					}
				}else if(type.equalsIgnoreCase("teacher")) {

				}


			}catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}

		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
}
