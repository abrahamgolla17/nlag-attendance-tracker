package attendance.controller.impl;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import attendance.controller.AttendanceController;
import attendance.model.Grade;
import attendance.model.Student;
import attendance.model.Teacher;
import attendance.service.AttendanceService;
import attendance.util.Response;

@RestController
public class AttendanceControllerImpl implements AttendanceController{

	@Autowired
	private AttendanceService attendanceService;

	private Logger logger = LogManager.getLogger(AttendanceControllerImpl.class);

	@RequestMapping(path = "/login", method = RequestMethod.POST,produces =  MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> login(
			@RequestHeader(name = "Authorization")  String authorization,
			@RequestParam(name = "appid") String appId) {

		logger.info("Login api invoked with appid "+appId);

		Response response = new Response();

		if(appId == null || appId.isEmpty()) {
			response.setData("App Id cannot be null");
			response.setStatus("FAILURE");
			response.setStatusCode(400);
			return ResponseEntity.badRequest().body(response);
		}

		if(authorization != null && authorization.contains("Basic")) {
			byte[] decodedBytes = Base64.getDecoder().decode(authorization.substring("Basic".length()).trim());
			String decodedValue = new String(decodedBytes);

			try {
				Map<String, Object> responseContent = attendanceService.validateUserAndGetData(decodedValue);
				if(responseContent != null) {
					response.setData(responseContent);
					response.setStatus("SUCCESS");
					response.setStatusCode(200);
					return ResponseEntity.ok().body(response);
				}else {
					response.setData("Please enter valid credentials");
					response.setStatus("FAILURE");
					response.setStatusCode(401);
					return ResponseEntity.badRequest().body(response);
				}
			}catch (Exception e) {
				logger.error(e.getLocalizedMessage());
				response.setData(e.getLocalizedMessage());
				response.setStatus("FAILURE");
				response.setStatusCode(503);
				return ResponseEntity.badRequest().body(response);
			}
		}else {
			response.setData("Authorization cannot be empty");
			response.setStatus("FAILURE");
			response.setStatusCode(503);
			return ResponseEntity.badRequest().body(response);
		}

	}

	@RequestMapping(path = "/students/{teacherid}", method = RequestMethod.GET,produces =  MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> getStudentData(@PathVariable(name = "teacherid")String teacherId,
			@RequestParam(name = "classid") String classId) {
		logger.info("Get student data by teacher with "+teacherId +" of class with Id " +classId );

		Response response = new Response();

		if(teacherId == null || classId == null) {
			response.setData("TeacherId or ClassId cannot be null");
			response.setStatus("FAILURE");
			response.setStatusCode(401);
			return ResponseEntity.badRequest().body(response);
		}
		try {
			if(attendanceService.validateTeacher(teacherId) != null && attendanceService.validateClass(classId) != null) {
				List<Student> students = attendanceService.getStudentsByClassId(classId);
				response.setData(students);
				response.setStatus("SUCCESS");
				response.setStatusCode(200);
				return ResponseEntity.ok().body(response);
			}else {
				response.setData("Invalid Class or Invalid Teacher Id");
				response.setStatus("FAILURE");
				response.setStatusCode(401);
				return ResponseEntity.badRequest().body(response);
			}
		}catch (Exception e) {
			logger.error(e.getLocalizedMessage());
			response.setData(e.getLocalizedMessage());
			response.setStatus("FAILURE");
			response.setStatusCode(503);
			return ResponseEntity.badRequest().body(response);
		}

	}

	@RequestMapping(path = "/attendance/{teacherid}", method = RequestMethod.POST,produces =  MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> recordAttendance(
			@PathVariable(name = "teacherid") String teacherId,
			@RequestParam(name = "classid") String classId,
			@RequestBody String studentData,
			@RequestParam(defaultValue = "false", required = false) boolean bulk) {

		logger.info("recording the attendance "+teacherId +" of class with Id " +classId );

		Response response = new Response();

		if(teacherId == null && classId == null && studentData == null) {
			response.setData("TeacherId or ClassId or student data cannot be null");
			response.setStatus("FAILURE");
			response.setStatusCode(401);
			return ResponseEntity.badRequest().body(response);
		}

		try {
			Teacher teacher = attendanceService.validateTeacher(teacherId);
			Grade grade = attendanceService.validateClass(classId);
			if(teacher != null &&  grade != null) {
				JSONObject object = new JSONObject(studentData);
				boolean isRecorded = attendanceService.recordAttendance(object, teacher, grade);
				if(isRecorded) {
					response.setData("Recorded Attendance");
					response.setStatus("SUCCESS");
					response.setStatusCode(200);
					return ResponseEntity.ok().body(response);
				}
			}else {
				response.setData("Invalid Class or Invalid Teacher Id");
				response.setStatus("FAILURE");
				response.setStatusCode(401);
				return ResponseEntity.badRequest().body(response);
			}
		}catch (Exception e) {
			logger.error(e);
			response.setData(e.getLocalizedMessage());
			response.setStatus("FAILURE");
			response.setStatusCode(503);
			return ResponseEntity.badRequest().body(response);
		}

		return null;
	}


	@Override
	public ResponseEntity<String> addUserData(String userId, String userData, boolean teacher, boolean bulk) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ResponseEntity<String> getStatistics(String userId, String year, String month, String studentId,
			String teacherId) {
		// TODO Auto-generated method stub
		return null;
	}



}
