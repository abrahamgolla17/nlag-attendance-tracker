package attendance.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import attendance.util.Response;

public interface CommonController {
	public ResponseEntity<Response> imageUpload(String authHeader, String studentId,MultipartFile file, HttpServletRequest request, boolean console);
	public ResponseEntity<Response> uploadData(String authHeader,String type,MultipartFile file, HttpServletRequest request, boolean console);
}
