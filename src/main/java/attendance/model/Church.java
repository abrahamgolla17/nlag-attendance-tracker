package attendance.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "CHURCH")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Church implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CHURCH_ID")
	private String churchId;
	
	@Column(name = "CHURCH_NAME")
	private String name;
	
	@Column(name = "CHURCH_ADDRESS")
	private String address;
	
	@Column(name = "CHURCH_GEO_COORDS")
	private String churchGeoCoordinates;
	
	@Column(name = "CREATED_TIMESTAMP")
	private long createdTimeStamp;
	
	@Column(name = "UPDATED_TIMESTAMP")
	private long updatedDateStamp;

	@OneToMany(mappedBy = "church", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Campus> campuses;
	
	public List<Campus> getCampuses() {
		return campuses;
	}

	public void setCampuses(List<Campus> campuses) {
		this.campuses = campuses;
	}

	public String getChurchId() {
		return churchId;
	}

	public void setChurchId(String churchId) {
		this.churchId = churchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	public long getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(long createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public String getChurchGeoCoordinates() {
		return churchGeoCoordinates;
	}

	public void setChurchGeoCoordinates(String churchGeoCoordinates) {
		this.churchGeoCoordinates = churchGeoCoordinates;
	}

	public long getUpdatedDateStamp() {
		return updatedDateStamp;
	}

	public void setUpdatedDateStamp(long updatedDateStamp) {
		this.updatedDateStamp = updatedDateStamp;
	}

	@Override
	public String toString() {
		return "Church [churchId=" + churchId + ", name=" + name + ", address=" + address + ", churchGeoCoordinates="
				+ churchGeoCoordinates + ", createdTimeStamp=" + createdTimeStamp + ", updatedDateStamp="
				+ updatedDateStamp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((churchGeoCoordinates == null) ? 0 : churchGeoCoordinates.hashCode());
		result = prime * result + ((churchId == null) ? 0 : churchId.hashCode());
		result = prime * result + (int) (createdTimeStamp ^ (createdTimeStamp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (updatedDateStamp ^ (updatedDateStamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Church other = (Church) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (churchGeoCoordinates == null) {
			if (other.churchGeoCoordinates != null)
				return false;
		} else if (!churchGeoCoordinates.equals(other.churchGeoCoordinates))
			return false;
		if (churchId == null) {
			if (other.churchId != null)
				return false;
		} else if (!churchId.equals(other.churchId))
			return false;
		if (createdTimeStamp != other.createdTimeStamp)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (updatedDateStamp != other.updatedDateStamp)
			return false;
		return true;
	}

	
}
