package attendance.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "CAMPUS")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Campus implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CAMPUS_ID")
	private String campusId;
	
	@Column(name = "CAMPUS_NAME")
	private String campusName;
	
	@Column(name = "CAMPUS_ADDRESS")
	private String campusAddress;
	
	@Column(name = "CAMPUS_GEO_COORDS")
	private String campusGeoCoordinates;
	
	@Column(name = "CREATED_TIMESTAMP")
	private long createdTimeStamp;
	
	@Column(name = "UPDATED_TIMESTAMP")
	private long updatedDateStamp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHURCH_ID", nullable = false)
	private Church church;
	
	@OneToMany(mappedBy = "campus", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Grade> grades;
	
	public List<Grade> getGrades() {
		return grades;
	}

	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCampusId() {
		return campusId;
	}

	public void setCampusId(String campusId) {
		this.campusId = campusId;
	}

	public String getCampusName() {
		return campusName;
	}

	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}

	public String getCampusAddress() {
		return campusAddress;
	}

	public void setCampusAddress(String campusAddress) {
		this.campusAddress = campusAddress;
	}

	public String getCampusGeoCoordinates() {
		return campusGeoCoordinates;
	}

	public void setCampusGeoCoordinates(String campusGeoCoordinates) {
		this.campusGeoCoordinates = campusGeoCoordinates;
	}

	public long getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(long createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public long getUpdatedDateStamp() {
		return updatedDateStamp;
	}

	public void setUpdatedDateStamp(long updatedDateStamp) {
		this.updatedDateStamp = updatedDateStamp;
	}
	
	public Church getChurch() {
		return church;
	}

	public void setChurch(Church church) {
		this.church = church;
	}

	@Override
	public String toString() {
		return "Campus [campusId=" + campusId + ", campusName=" + campusName + ", campusAddress=" + campusAddress
				+ ", campusGeoCoordinates=" + campusGeoCoordinates + ", createdTimeStamp=" + createdTimeStamp
				+ ", updatedDateStamp=" + updatedDateStamp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campusAddress == null) ? 0 : campusAddress.hashCode());
		result = prime * result + ((campusGeoCoordinates == null) ? 0 : campusGeoCoordinates.hashCode());
		result = prime * result + ((campusId == null) ? 0 : campusId.hashCode());
		result = prime * result + ((campusName == null) ? 0 : campusName.hashCode());
		result = prime * result + (int) (createdTimeStamp ^ (createdTimeStamp >>> 32));
		result = prime * result + (int) (updatedDateStamp ^ (updatedDateStamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campus other = (Campus) obj;
		if (campusAddress == null) {
			if (other.campusAddress != null)
				return false;
		} else if (!campusAddress.equals(other.campusAddress))
			return false;
		if (campusGeoCoordinates == null) {
			if (other.campusGeoCoordinates != null)
				return false;
		} else if (!campusGeoCoordinates.equals(other.campusGeoCoordinates))
			return false;
		if (campusId == null) {
			if (other.campusId != null)
				return false;
		} else if (!campusId.equals(other.campusId))
			return false;
		if (campusName == null) {
			if (other.campusName != null)
				return false;
		} else if (!campusName.equals(other.campusName))
			return false;
		if (createdTimeStamp != other.createdTimeStamp)
			return false;
		if (updatedDateStamp != other.updatedDateStamp)
			return false;
		return true;
	}

	

}
