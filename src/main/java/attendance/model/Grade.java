package attendance.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "CLASS")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Grade implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "CLASS_ID")
	private String classId;
	
	@Column(name = "CLASS_NAME")
	private String className;
	
	@Column(name = "CREATED_TIMESTAMP")
	private long createdTimeStamp;
	
	@Column(name = "UPDATED_TIMESTAMP")
	private long updatedDateStamp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CAMPUS_ID")
	private Campus campus;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(
			name = "CLASS_TEACHER",
			joinColumns = {@JoinColumn(name="CLASS_ID")},
			inverseJoinColumns = {@JoinColumn(name="TEACHER_ID")})
	private Set<Teacher> teachers;
	
	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public long getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(long createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public long getUpdatedDateStamp() {
		return updatedDateStamp;
	}

	public void setUpdatedDateStamp(long updatedDateStamp) {
		this.updatedDateStamp = updatedDateStamp;
	}
	
	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public Set<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}

	@Override
	public String toString() {
		return "Grade [classId=" + classId + ", className=" + className + ", createdTimeStamp=" + createdTimeStamp
				+ ", updatedDateStamp=" + updatedDateStamp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classId == null) ? 0 : classId.hashCode());
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + (int) (createdTimeStamp ^ (createdTimeStamp >>> 32));
		result = prime * result + (int) (updatedDateStamp ^ (updatedDateStamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grade other = (Grade) obj;
		if (classId == null) {
			if (other.classId != null)
				return false;
		} else if (!classId.equals(other.classId))
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (createdTimeStamp != other.createdTimeStamp)
			return false;
		if (updatedDateStamp != other.updatedDateStamp)
			return false;
		return true;
	}

	
}
