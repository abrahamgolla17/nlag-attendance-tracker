package attendance.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT_ATTENDANCE")
public class StudentAttendance implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "STUDENT_ATTID")
	private String studentAttendanceId;
	
	@Column(name = "DATE")
	private Date date;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "YEAR")
	private int year;
	
	@Column(name = "W1")
	private String week1;
	
	@Column(name = "W2")
	private String week2;
	
	@Column(name = "W3")
	private String week3;
	
	@Column(name = "W4")
	private String week4;
	
	@Column(name = "W5")
	private String week5;
	
	@Column(name = "M1")
	private String miscellaneousWeek1;
	
	@Column(name = "M2")
	private String miscellaneousWeek2;
	
	@Column(name = "M3")
	private String miscellaneousWeek3;
	
	@Column(name = "M4")
	private String miscellaneousWeek4;
	
	@Column(name = "M5")
	private String miscellaneousWeek5;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STUDENT_ID")
	private Student student;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLASS_ID")
	private Grade grade;

	public String getStudentAttendanceId() {
		return studentAttendanceId;
	}

	public void setStudentAttendanceId(String studentAttendanceId) {
		this.studentAttendanceId = studentAttendanceId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getWeek1() {
		return week1;
	}

	public void setWeek1(String week1) {
		this.week1 = week1;
	}

	public String getWeek2() {
		return week2;
	}

	public void setWeek2(String week2) {
		this.week2 = week2;
	}

	public String getWeek3() {
		return week3;
	}

	public void setWeek3(String week3) {
		this.week3 = week3;
	}

	public String getWeek4() {
		return week4;
	}

	public void setWeek4(String week4) {
		this.week4 = week4;
	}

	public String getWeek5() {
		return week5;
	}

	public void setWeek5(String week5) {
		this.week5 = week5;
	}

	public String getMiscellaneousWeek1() {
		return miscellaneousWeek1;
	}

	public void setMiscellaneousWeek1(String miscellaneousWeek1) {
		this.miscellaneousWeek1 = miscellaneousWeek1;
	}

	public String getMiscellaneousWeek2() {
		return miscellaneousWeek2;
	}

	public void setMiscellaneousWeek2(String miscellaneousWeek2) {
		this.miscellaneousWeek2 = miscellaneousWeek2;
	}

	public String getMiscellaneousWeek3() {
		return miscellaneousWeek3;
	}

	public void setMiscellaneousWeek3(String miscellaneousWeek3) {
		this.miscellaneousWeek3 = miscellaneousWeek3;
	}

	public String getMiscellaneousWeek4() {
		return miscellaneousWeek4;
	}

	public void setMiscellaneousWeek4(String miscellaneousWeek4) {
		this.miscellaneousWeek4 = miscellaneousWeek4;
	}

	public String getMiscellaneousWeek5() {
		return miscellaneousWeek5;
	}

	public void setMiscellaneousWeek5(String miscellaneousWeek5) {
		this.miscellaneousWeek5 = miscellaneousWeek5;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}
}
