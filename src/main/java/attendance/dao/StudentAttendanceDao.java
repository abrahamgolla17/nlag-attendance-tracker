package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.StudentAttendance;

@Repository
public interface StudentAttendanceDao extends CrudRepository<StudentAttendance, String>{
	
//	public StudentAttendance findByGrade_ClassIdAndStudent_StudentIdAndStudentAttendance_month(String classId, String studentId, String month);
	public StudentAttendance findByMonthAndGrade_ClassIdAndStudent_StudentId(String month,String classId, String studentId);

}
