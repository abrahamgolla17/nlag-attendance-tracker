package attendance.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Student;

@Repository
public interface StudentDao extends CrudRepository<Student, String>{

	public Student findBystudentId(String studentId);
	public List<Student> findByGrade_classId(String classId);
}
