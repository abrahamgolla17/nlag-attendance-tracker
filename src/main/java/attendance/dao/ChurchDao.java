package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Church;

@Repository
public interface ChurchDao extends CrudRepository<Church, String>{

}
