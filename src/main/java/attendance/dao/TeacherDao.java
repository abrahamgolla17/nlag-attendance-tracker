package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Teacher;

@Repository
public interface TeacherDao extends CrudRepository<Teacher, String>{

	public Teacher findByphoneNumber(String mobileNumber);
	public Teacher findByTeacherId(String teacherId);
}
