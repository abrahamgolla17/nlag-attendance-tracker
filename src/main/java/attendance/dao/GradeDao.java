package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Grade;

@Repository
public interface GradeDao extends CrudRepository<Grade, String> {

	public Grade findByclassName(String className);
}
