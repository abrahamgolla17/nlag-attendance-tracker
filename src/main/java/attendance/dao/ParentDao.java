package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Parent;

@Repository
public interface ParentDao extends CrudRepository<Parent, String>{
	
	public Parent findByphoneNumber(String phoneNumber);
	public Parent findByFirstNameAndLastName(String firstName, String lastName);
 
}
