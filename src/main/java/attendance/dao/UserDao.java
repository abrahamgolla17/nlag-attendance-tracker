package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.UserAuth;

@Repository
public interface UserDao extends CrudRepository<UserAuth, String>{
	public UserAuth findBymobileNumber(String mobileNumber);
}
