package attendance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import attendance.model.Campus;

@Repository
public interface CampusDao extends CrudRepository<Campus, String>{

}
